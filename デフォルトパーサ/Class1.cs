﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Collections;


namespace デフォルトパーサ
{
    public class Class1
    {
        public Hashtable doParse(Hashtable result) 
        {
            string html = (string)result[result.Count];
            //HTMlパース
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);

            string title = doc.DocumentNode.SelectNodes(@"//title")[0].InnerText.Replace("\r", "").Replace("\n", "").Replace("\t", "").Trim();
            
            result = new Hashtable();
            result["title"] = title;
            result["body"] = html;

            return result;

        }
    }
}
