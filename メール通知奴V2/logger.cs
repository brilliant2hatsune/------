﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace メール通知奴
{
    static class Logger
    {
        static string logPath = @"c:\test.log";
        public static void setLog(string filepath){
            logPath = filepath;
        }
        
        public static void log(string log)
        {
            System.IO.File.AppendAllText(logPath, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " " + log + Environment.NewLine);
        }
        public static void log(int log)
        {
            System.IO.File.AppendAllText(logPath, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " " + log + Environment.NewLine);
        }
        
    }
}
